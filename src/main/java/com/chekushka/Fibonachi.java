package com.chekushka;

/**
 * Клас для відображення числ Фібоначі.
 *
 * @author Serhii Chekun
 * @see App
 */
class Fibonachi {
    /**
     * Метод який друкує числа в інтервалі. Непарні в  порядку зростання.
     * Парні навпаки. Показує їню суму.
     *
     * @param F1 - максимальне значення непарного числа
     * @param F2 - максимальне значення парного числа
     * @param N - кількість чисел
     *           Значення вказуються в головному класі.
     * @see App
     */
    void PrintFibonachi(int F1, int F2, int N) {
        int biggest = Math.max(F1, F2);
        int sumNumber = 1;
        int temp = 0;
        int oddCount = 0;
        int evenCount = 0;
        float oddPersent;
        float evenPersent;
        System.out.println("Fibonachi numbers");
        for (int i = 1, j = 1; i <= biggest || sumNumber <= N; ) {
            temp = i + j;
            System.out.println(i);
            if ((i % 2) == 1) {
                oddCount++;
            } else {
                evenCount++;
            }
            i = j;
            j = temp;
            sumNumber++;

        }
        oddPersent = (float) (oddCount * 100) / sumNumber;
        evenPersent = (float) (evenCount * 100) / sumNumber;
        System.out.println("Odd numbers:" + oddPersent + "%");
        System.out.println("Even numbers:" + evenPersent + "%");
    }
}
