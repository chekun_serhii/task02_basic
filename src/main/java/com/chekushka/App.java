package com.chekushka;

import java.util.Scanner;

/**
 * Головний клас програми. Запускає продукт та дозволяє обрати потрібну вам фукцію.
 *
 * @version 0.1.3
 * @author Serhii Chekun
 * @see OddEvenNumber
 * @see Fibonachi
 */
public class App {
    /**
     * Метод для запуску програми
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Select an option:\n1. Odd and even numbers.\n2.Fibonachi algorithm\n3.Exit");
            System.out.println("Enter here:");
            int choice = scanner.nextInt();
            switch (choice) {
                case (1):
                    System.out.println("Enter interval of numbers. Do not make the interval too long.");
                    System.out.println("If you are not \"suicide\", ofc)");
                    System.out.println("Enter minimal number:");
                    int min = scanner.nextInt();
                    System.out.println("Enter maximal number:");
                    int max = scanner.nextInt();
                    OddEvenNumber test = new OddEvenNumber();
                    test.PrintNumber(min, max);
                    break;
                case (2):
                    int F1 = 233;
                    int F2 = 144;
                    System.out.println("Enter the number of numbers");
                    int numb = scanner.nextInt();
                    Fibonachi fTest = new Fibonachi();
                    fTest.PrintFibonachi(F1, F2, numb);
                    break;
                case (3):
                    System.exit(0);
                    break;
                default:
                    System.out.println("There is no option with this number");
                    break;
            }
        }
    }
}
