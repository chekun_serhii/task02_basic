package com.chekushka;

/**
 * Клас для відображення парних і непарних чисел.
 *
 * @author Serhii Chekun
 * @see App
 */
public class OddEvenNumber {
    /**
     * Метод який друкує числа в інтервалі. Непарні в  порядку зростання.
     * Парні навпаки. Показує їню суму.
     *
     * @param minimal - мінімальне значення інтервалу
     * @param maximal - максимальне значення інтервалу
     *                Значення вказуються в головному класі
     * @see App
     */
    final void PrintNumber(int minimal,int maximal) {
        int startOdd;
        int startEven;
        int oddSum = 0;
        int evenSum = 0;
        if ((minimal % 2) == 1) {
            startOdd = minimal;
            if ((maximal % 2) == 1) {
                startEven = maximal--;
            } else {
                startEven = maximal;
            }
        } else {
            startOdd = minimal++;
            if ((maximal % 2) == 0) {
                startEven = maximal;
            } else {
                startEven = maximal--;
            }
        }
        System.out.println("Odd numbers");
        for (int i = startOdd; i <= maximal;) {
            System.out.println(i);
            oddSum += 1;
            i = i + 2;
        }
        System.out.println("Even numbers");
        for (int i = startEven; i >= minimal;) {
            System.out.println(i);
            evenSum += 1;
            i = i - 2;
        }
        System.out.println("Sum of odd numbers: " + oddSum);
        System.out.println("Sum of even numbers: " + evenSum);
    }
}
